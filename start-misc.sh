#!/bin/sh

IRIS_NODE=/sys/module/radio_iris_transport/parameters/fmsmd_set
VIBRATOR_NODE=/sys/class/timed_output/vibrator/enable
chown root:video "$IRIS_NODE"
chmod g+w "$IRIS_NODE"
chown root:audio "$VIBRATOR_NODE"
chmod g+w "$VIBRATOR_NODE"
