#!/bin/sh
# Nokia 8110 device startup package
# Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# rmnet0 is the physical interface. It is connected to the modem via bam-dmux
ifconfig rmnet0 up
# rmnet0 is in Ethernet mode by default, but the modem is in IP mode
# The modem can't be switched to Ethernet mode on msm8909, at least on the 8110
rmnet-setdatamode rmnet0 ip
# Create rmnet_data0, which will be used by ofono. Link it to rmnet0
rmnet-createvnd rmnet0
