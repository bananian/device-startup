#!/bin/sh
# Nokia 8110 device startup package
# Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

modprobe hci_smd
modprobe bluetooth
modprobe bnep
modprobe hidp
modprobe rfcomm
echo 1 > /sys/module/hci_smd/parameters/hcismd_set # attach SMD channels
