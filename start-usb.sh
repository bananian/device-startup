#!/bin/sh
# Nokia 8110 device startup package
# Copyright (C) 2020-2021 Affe Null <affenull2345@gmail.com>
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
ANDROID_USB=/sys/class/android_usb/android0

echo 0 > $ANDROID_USB/enable
echo 1 > /sys/class/power_supply/battery/charging_enabled
(cat /etc/android-usb-functions || echo rndis) > $ANDROID_USB/functions
echo 1 > $ANDROID_USB/enable
